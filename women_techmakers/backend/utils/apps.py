from django.apps import AppConfig


class UtilsConfig(AppConfig):
    name = "women_techmakers.backend.utils"
    verbose_name = 'utilities'
