from django.apps import AppConfig


class AboutConfig(AppConfig):
    name = 'women_techmakers.backend.about'
    verbose_name = "About Us"
