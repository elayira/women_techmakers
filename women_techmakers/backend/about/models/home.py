from django.db import models
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailcore.models import Page, Orderable
from modelcluster.fields import ParentalKey
from women_techmakers.backend.utils.models import CardBlurb


class EventCard(Orderable, CardBlurb):
    page = ParentalKey('about.Home', related_name='initiative_cards')


class StoryCard(Orderable, CardBlurb):
    page = ParentalKey('about.Home', related_name='story_cards')


class Home(Page):
    intro = RichTextField(blank=True)
    more_button = models.URLField(blank=True)

    video_embed = RichTextField(blank=True, features=['embed'])
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    banner_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    first_tag_board_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    second_tag_board_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    initiatives_brief = RichTextField(blank=True)
    stories_brief = RichTextField(blank=True)

    tag_board_tag = RichTextField(blank=True)
    tag_board_body = RichTextField(blank=True)
    tag_board_button = models.URLField(blank=True)

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel('intro', classname='full'),
                FieldPanel('more_button'),
            ],
            heading="Hero Section", classname="collapsible collapsed"
        ),
        MultiFieldPanel(
            [
                FieldPanel('initiatives_brief', classname='full'),
                InlinePanel('initiative_cards')
            ],
            heading="our Initiatives", classname="collapsible collapsed"
        ),
        MultiFieldPanel(
            [
                FieldPanel('stories_brief', classname='full'),
                InlinePanel('story_cards')
            ],
            heading="Our Stories", classname="collapsible collapsed"
        ),
        MultiFieldPanel(
            [
                FieldPanel('tag_board_tag', classname='full'),
                FieldPanel('tag_board_body', classname='full'),
                FieldPanel('tag_board_button', classname='full'),
                ImageChooserPanel('first_tag_board_image'),
                ImageChooserPanel('second_tag_board_image'),
            ],
            heading="Tag Board", classname="collapsible collapsed"
        )
    ]
    promote_panels = Page.promote_panels + [
        MultiFieldPanel(
            [
                ImageChooserPanel('feed_image'),
                ImageChooserPanel('banner_image'),
                FieldPanel('video_embed')

            ],
            heading="Media Feeds",
            classname="collapsible collapsed"
        )
    ]
