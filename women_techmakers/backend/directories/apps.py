from django.apps import AppConfig


class DirectoriesConfig(AppConfig):
    name = "women_techmakers.backend.directories"
    verbose_name = 'directories'
